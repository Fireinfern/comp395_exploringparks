﻿using System;
using TMPro;
using UnityEngine;

namespace UI
{
    public class InformationUIComponent : MonoBehaviour
    {
        [SerializeField] private TMP_Text textComponent;

        [SerializeField, TextArea] private string informationText;

        private void Start()
        {
            textComponent.SetText(informationText);
        }
    }
}